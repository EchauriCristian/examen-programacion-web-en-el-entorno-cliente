/*
Tenemos una clase controlButton que agrupa los dos botones para pasar de una Card de bootstrap a otra.
Tenemos un input hidden para guardar en que Card estamos en este momento.
Utilizamos la clase hidden para dar un valor none a display y aplicarselo a las Card's.
*/

$(document).on("click",".controlButton", function(){

    var current = $(".currentCard").val();

    if ($(this).val()=="next" && current!="third-card"){

        $("#"+current).addClass("hidden");

        switch(current){

            case "first-card": 
                $("#second-card").removeClass("hidden");
                $(".currentCard").val("second-card");
            break;

            case "second-card":
                $("#third-card").removeClass("hidden");
                $(".currentCard").val("third-card");
            break;
        }
    }
    
    else if($(this).val()=="prev" && current!="first-card"){

        $("#"+current).addClass("hidden");

        switch(current){

            case "second-card": 
                $("#first-card").removeClass("hidden");
                $(".currentCard").val("first-card");
            break;
    
            case "third-card":
                $("#second-card").removeClass("hidden");
                $(".currentCard").val("second-card");
            break;
        }
    }
});